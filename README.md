# Electronic aids for the CSE session exam

```
.
├── Homeworks
│   ├── hpcse_I
│   │   ├── solution01.pdf
│   │   ├── solution02.pdf
│   │   ├── solution03.pdf
│   │   ├── solution04.pdf
│   │   ├── solution05.pdf
│   │   ├── solution06.pdf
│   │   ├── solution07.pdf
│   │   ├── solution08.pdf
│   │   ├── solution09.pdf
│   │   ├── solution10.pdf
│   │   ├── solution11.pdf
│   │   └── solution12.pdf
│   └── hpcse_II
│       ├── solution01.pdf
│       ├── solution02.pdf
│       ├── solution03a.pdf
│       ├── solution03b.pdf
│       ├── solution04.pdf
│       └── solution05.pdf
├── LectureNotes
│   ├── hpcse_I
│   │   ├── HPCSE_I_amdahl_gustafson.pdf
│   │   ├── HPCSE_I_NN1_Linear_Features_lecture_notes.pdf
│   │   ├── HPCSE_I_NN2_Backpropagation_lecture_notes.pdf
│   │   ├── HPCSE_I_PCA_lecture_notes.pdf
│   │   └── HPCSE_I_PSE_lecture_notes.pdf
│   ├── hpcse_II
│   │   └── HPCSE_II_uq_notes.pdf
│   └── ParallelProgramming_Eijkhout.pdf
├── LectureSlides
│   ├── hpcse_I
│   │   ├── HPCSE_I_01_Intro.pdf
│   │   ├── HPCSE_I_02_SharedMemoryIntro.pdf
│   │   ├── HPCSE_I_03_SharedMemoryOpenMP_1.pdf
│   │   ├── HPCSE_I_04_OpenMP_2_Vec_ISPC.pdf
│   │   ├── HPCSE_I_05_HPC_LinAlg_Blas.pdf
│   │   ├── HPCSE_I_06_PCA.pdf
│   │   ├── HPCSE_I_08_DistributedMemoryMPI_1.pdf
│   │   ├── HPCSE_I_09_DistributedMemoryMPI_2.pdf
│   │   ├── HPCSE_I_10_DistributedMemoryMPI_3.pdf
│   │   ├── HPCSE_I_11_Diffusion.pdf
│   │   └── HPCSE_I_13_Particles.pdf
│   └── hpcse_II
│       ├── HPCSE_II_01_Introduction.pdf
│       ├── HPCSE_II_02_UQ_Intro.pdf
│       ├── HPCSE_II_03_UQ_Extra_Slides.pdf
│       ├── HPCSE_II_04_Korali.pdf
│       ├── HPCSE_II_07_UPC.pdf
│       ├── HPCSE_II_08_CUDA.pdf
│       ├── HPCSE_II_09_CUDA_Optimizations.pdf
│       ├── HPCSE_II_10_Advanced_MPI.pdf
│       └── HPCSE_II_11_Communication_Tolerant_Programming.pdf
├── Manuals
│   ├── cppreference
│   │   └── cpp_reference_html_book_20180311.tar.gz
│   ├── cuda
│   │   └── CUDA_QuickReference.pdf
│   ├── intel_intrinsics
│   │   ├── Intrinsics_Guide_for_Intel.sh
│   │   └── jnix_license.txt
│   ├── mpi
│   │   ├── hpcse_I
│   │   │   ├── mpi31-report.pdf
│   │   │   └── MPIquickref.pdf
│   │   └── hpcse_II
│   │       └── mpi-quick-ref.pdf
│   ├── openmp
│   │   ├── openmp-4.5.pdf
│   │   └── openmp-4.5-refcard.pdf
│   └── upcxx
│       └── upcxx-cheatsheet.pdf
├── README.md
└── Supplementary
    ├── cmaes.pdf
    ├── tmcmc.pdf
    └── upc++ Bekeley.pdf
```
